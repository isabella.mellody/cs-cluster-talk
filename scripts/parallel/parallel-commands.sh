#!/bin/bash
#SBATCH -N 3 # 3 nodes
#SBATCH -n 3 # 3 tasks
#SBATCH --cpus-per-task=1
#SBATCH -t 00:01:00
# Execute jobs in parallel
srun -N 1 -n 1 hostname &
srun -N 1 -n 1 hostname &
srun -N 1 -n 1 hostname &
wait

