#!/bin/bash
#SBATCH -o  /usr/xtmp/wjs/slurm-%j.out -e  /usr/xtmp/wjs/slurm-%j.err
#SBATCH -a 1-50 --ntasks-per-node=1
srun python3 montepi.py -c -n 100000000
