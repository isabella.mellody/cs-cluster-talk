#!/bin/bash
#SBATCH -t 0:05:00  # time requested in hour:minute:second
#SBATCH -o /usr/xtmp/wjs/slurm-%j.out
#SBATCH -e /usr/xtmp/wjs/slurm-%j.err
#SBATCH --mem=1G
#SBATCH -p compsci-gpu --gres=gpu:2 --array=1-100
hostname
