#!/bin/bash
# We need 40 cores
#SBATCH -c 40      # cores requested
# Limit the time to 5 minutes for good housekeeping
#SBATCH -t 0:05:00  # time requested in hour:minute:second
# Define our output and error files
#SBATCH -o /usr/xtmp/wjs/slurm-%j.out
#SBATCH -e /usr/xtmp/wjs/slurm-%j.err
# Request our memory
#SBATCH --mem=1G
hostname
