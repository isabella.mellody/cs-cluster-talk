#!/bin/bash
#SBATCH -t 0:05:00  # time requested in hour:minute:second
##SBATCH --mem=1G
#SBATCH --gres=gpu:1
#SBATCH --partition=compsci-gpu
hostname && nvidia-smi && env
